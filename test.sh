#!/bin/bash 

rm ./dysk1
echo creating new disk 
./vcreate dysk1 10

echo dysk1 info:
./vinfo dysk1

echo files in dysk1:
./vls dysk1

echo adding some files:
./vadd dysk1 1kB 1
./vadd dysk1 1kB 2
./vadd dysk1 1kB 3
./vadd dysk1 1kB 4
./vadd dysk1 1kB 5
./vadd dysk1 1kB 6
./vadd dysk1 1kB 7
./vadd dysk1 1kB 8
./vadd dysk1 1kB 9

echo dysk1 info:
./vinfo dysk1

echo files in dysk1:
./vls dysk1

echo rm some files:
./vrm dysk1 1
./vrm dysk1 3
./vrm dysk1 5
./vrm dysk1 7

echo files in dysk1:
./vls dysk1

echo dysk1 info:
./vinfo dysk1

echo trying to add 2kB file, wont work
./vadd dysk1 2kB x

echo files in dysk1:
./vls dysk1

echo dysk1 info:
./vinfo dysk1
