#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 3){
        printf("usage: \n\tvcreate   <filename>   <size in kB>\n");
        printf("\tvcreate   <filename>   <size in kB>   <name of VD>\n");
        fflush(stdin);
        return -1;
    }

    char *plik;
    plik = argv[1];

    if( access( plik, F_OK ) != -1 ) {
        printf("file \"%s\" exists! cannot create VD\n", plik);
        return -1;
    }

    int size__bit = 8000 * atoi(argv[2]);

    char *name;
    if(argc==4)
        name = argv[3];
    else
        name = plik;
    if(makeVD(plik, size__bit, name)<0){
        printf("cannot make VD\n");
        return -1;
    }
    return(0);
}
