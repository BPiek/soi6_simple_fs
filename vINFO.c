#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 2){
        printf("usage: \n\tvinfo  <filename(VD)>   \n");
        fflush(stdin);
        return -1;
    }

    char *vd;
    vd = argv[1];

    if( access( vd, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"(VD)! \n", vd);
        return -1;
    }

    size_t size__bit;
    size_t free__bit;
    char* id = malloc(sizeof(char)*MAX_DISK_ID_SIZE);
    if( GetVD_SB_SIZE(argv[1],&size__bit)<0 ||
        GetVD_SB_ID(vd, id) <0 ||
        GetVD_SB_FREE(vd, &free__bit)<0){
        printf("Cannot perform info action on vd %s\n", vd);
        return -1;
    }


    printf("ID:\t%s\nSize:\t%.2f kB\nFree:\t%.2f kB\n", id,size__bit/(float)8000,free__bit/(float)8000);


    return(0);
}
