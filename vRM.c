#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 3){
        printf("usage: \n\tvrm   <filename(VD)>   <filename(file to rm)>\n");
        fflush(stdin);
        return -1;
    }

    char *vd;
    vd = argv[1];

    if( access( vd, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"(VD)! \n", vd);
        return -1;
    }

    char *fileToRm;
    fileToRm = argv[2];

    if( DeleteVD_VFILE(vd, fileToRm) <0){
        printf("cannot rm file %s, file does not exists!\n", fileToRm);
    }

    return(0);
}
