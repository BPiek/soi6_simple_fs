#include "types.h"
#include <stdlib.h>
#define DEBUG_TYPES

#ifdef DEBUG_TYPES
    #include <stdio.h>
#endif

void types_deb(char* c, int code){
    #ifdef DEBUG_TYPES
        printf("[TYPES] (%d): %s\n", code, c);
    #endif
}
/////////////////////
/// BIT implementation
/////////////////////
int createBitTable(int size, int** table){
    int sizeOfArray = (size/BITS_IN_INT);
    if(size%BITS_IN_INT > 0)
        sizeOfArray +=1;
    types_deb("createBitTable, size of array of int",sizeOfArray);

    int* x;
    x = (int*) calloc(sizeOfArray,sizeof(int));

    *table = x;
    if (table==NULL){
        types_deb("createBitTable: table not created ", -1);
        exit (1);
    }
    return sizeOfArray*BITS_IN_INT;
}

void SetBit(int *A, int k){
    A[k/32] |= 1 << (k%32);  // Set the bit at the k-th position in A[i]
}

void  ClearBit( int *A,  int k ){
    A[(k/32)] &= ~(1 << (k%32));
}

int TestBit( int *A,  int k ){
    return ( (A[k/32] & (1 << (k%32) )) != 0 ) ;
}

void printBitTable(int *A,int size){
    int i;
    printf("printing bit table ...\n\t");
    if(size <= 64){
        for(i =0; i<size; i++){
            printf("%d",i);
            if(i<100) printf(" ");
            if(i<10) printf(" ");
        }
        printf("\n\t");
    }
    for(i =0; i<size; i++){
        printf("%d  ", TestBit(A,i));
    }
    printf("\n");
}


void clearBitTable(int *A, int size)
{
    int i;
    for(i =0; i<size; i++){
        ClearBit(A,i);
    }
}
