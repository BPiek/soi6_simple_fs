#include "vdisk.h"
#include <time.h>

//#define DEBUG_VD

//#ifdef DEBUG_VD
    #include <stdio.h>
//#endif

void vd_deb(char* c, int code){
    #ifdef DEBUG_VD
        printf("[VD] (%d): %s\n", code, c);
    #else
        usleep(0); // w/o this there are some troubles sometimes (in vls programm)
                   // woth opening files (this workaround works by adding minimal
                   // delays in places I wanted to debug (critical places)
                   // #magic#programmingskill
    #endif

}

int GetVD_SB_ID(char *fileName, char* retName)
{
    vd_deb("GetVD_SB_ID starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        fseek(fp, 0, SEEK_SET);
        fread(retName, sizeof(char), MAX_DISK_ID_SIZE, fp);
    fclose(fp);

    vd_deb("GetVD_SB_ID starting done",0);
}


int GetVD_SB_SIZE(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_SIZE starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset = sizeof(char)*MAX_DISK_ID_SIZE;
        vd_deb("GetVD_SB_SIZE compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_SIZE got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_SIZE starting done",0);
    return 1;
}


int GetVD_SB_FREE(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_FREE starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset = sizeof(char)*MAX_DISK_ID_SIZE + sizeof(size_t)*1;
        vd_deb("GetVD_SB_SIZE compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_SIZE got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_FREE starting done",0);
    return 1;
}


int GetVD_SB_BC(char *fileName, unsigned int *ret)
{
    vd_deb("GetVD_SB_BC starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset = sizeof(char)*MAX_DISK_ID_SIZE + sizeof(size_t)*2;
        vd_deb("GetVD_SB_SIZE compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(unsigned int), 1, fp);
        vd_deb("GetVD_SB_SIZE got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_BC starting done",0);
    return 1;
}


int GetVD_SB_IC(char *fileName, unsigned int *ret)
{
    vd_deb("GetVD_SB_IC starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*2 +
                        sizeof(unsigned int)*1;
        vd_deb("GetVD_SB_IC compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(unsigned int), 1, fp);
        vd_deb("GetVD_SB_IC got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_IC starting done",0);
    return 1;
}


int GetVD_SB_1BM(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_1BM starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*2 +
                        sizeof(unsigned int)*2;
        vd_deb("GetVD_SB_1BM compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_1BM got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_1BM starting done",0);
    return 1;
}


int GetVD_SB_1IM(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_1IM starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*3 +
                        sizeof(unsigned int)*2;
        vd_deb("GetVD_SB_1IM compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_1IM got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_1IM starting done",0);
    return 1;
}


int GetVD_SB_1B(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_1B starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*4 +
                        sizeof(unsigned int)*2;
        vd_deb("GetVD_SB_1B compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_1B got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_1B starting done",0);
    return 1;
}


int GetVD_SB_1I(char *fileName, size_t *ret)
{
    vd_deb("GetVD_SB_1I starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*5 +
                        sizeof(unsigned int)*2;
        vd_deb("GetVD_SB_1I compute offset", offset);
        fseek(fp, offset, SEEK_SET);
        fread(ret, sizeof(size_t), 1, fp);
        vd_deb("GetVD_SB_1I got value", (*ret));
    fclose(fp);

    vd_deb("GetVD_SB_1I starting done",0);
    return 1;
}


int ChangeVD_SB_FREE(char *fileName, long blocks_number)
{
    vd_deb("ChangeVD_SB_FREE starting ...",0);

    FILE* fp;
    fp=fopen(fileName, "r+");
        if(fp == NULL){
            vd_deb("ERR: file not opened",-1);
            return -1;
        }
        int offset =    sizeof(char)*MAX_DISK_ID_SIZE +
                        sizeof(size_t)*1;
        vd_deb("ChangeVD_SB_FREE compute offset", offset);

        fseek(fp, offset, SEEK_SET);
        size_t *oldsize = malloc(sizeof(size_t));
        fread(oldsize, sizeof(size_t), 1, fp);
        vd_deb("ChangeVD_SB_FREE old size: ", (*oldsize));

        fseek(fp, offset, SEEK_SET);
        size_t newsize = ((*oldsize) + (blocks_number * BLOCK_SIZE__BIT));
        fwrite( &(newsize), sizeof(size_t),1,fp);
        vd_deb("ChangeVD_SB_FREE new size: ", (newsize));
        free(oldsize);
    fclose(fp);

    vd_deb("GetVD_SB_1I starting done",0);
    return 1;
}


int makeVD(char *fileName, size_t size, char *name) //size in bits
{

    vd_deb("makeVD starting >>>",0);
    // creating SB data structure
    vd_deb("makeVD creating SB ",0);
    SuperBlock_t *sb = (SuperBlock_t*) malloc(sizeof(SuperBlock_t));
    int err;

    // initialising SB
    vd_deb("makeVD init SB ",0);
    err = initSuperBlock(sb, size, name);
    if(err <0){
        free(sb);
        printf("ERR: makeVD");
        return err;
    }

    // initialising bit maps
    vd_deb("makeVD init bitmaps ",0);
    int *inode_bitmap, *block_bitmap;
    int err1,err2;
    err1 = createBitTable(sb->mInodeCount, &inode_bitmap);
    err2 = createBitTable(sb->mBlockCount, &block_bitmap);

    //for tests>> function t7 in main.c
//    SetBit(inode_bitmap, 1);
//    SetBit(inode_bitmap, 3);
//    SetBit(inode_bitmap, 20);


//    SetBit(block_bitmap, 2);
//    SetBit(block_bitmap, 9);
//    SetBit(block_bitmap, 50);

//    printBitTable(inode_bitmap, sb->mInodeCount);
//    printf("\n\n");
//    printBitTable(block_bitmap, sb->mBlockCount);
//    printf("\n\n");
    //<<

    if(err1<0 || err2<0){
        free(inode_bitmap);
        free(block_bitmap);
        printf("err init maps");
        return -1;
    }
    sb->mInodeCount = err1; // err1/2 are in fact no of elems (or err code if <0)
    sb->mBlockCount = err2;
//update of data
    sb->m1stBlockMap =  sb->m1stInodeMap + sb->mInodeCount/8;
    sb->m1stInode = sb->m1stBlockMap + sb->mBlockCount/8;
    sb->m1stBlock = sb->m1stInode + INODE_SIZE__BYTE*sb->mInodeCount;

    sb->mSize__bit =
            SUPER_BLOCK_SIZE__BYTE*8 +
            sb->mBlockCount*(BOOL_SIZE__BIT  + BLOCK_SIZE__BIT) +
            sb->mInodeCount*(BOOL_SIZE__BIT + INODE_SIZE__BYTE*8);
    sb->mFreeSize__bit = sb->mBlockCount * BLOCK_SIZE__BIT;

//    clearBitTable(inode_bitmap, sb->mInodeCount);
//    clearBitTable(block_bitmap, sb->mBlockCount);

    // writing SB
    vd_deb("makeVD write SB ",0);
    writeSuperBlock(sb,fileName);


//    printBitTable(inode_bitmap, sb->mInodeCount);
//    printBitTable(block_bitmap, sb->mBlockCount);

    if(err1 <0){
        free(sb);
        free(inode_bitmap); free(block_bitmap);
        printf("ERR: makeVD %d", err1);
        return err1;
    }
    if(err2 <0){
        free(sb);
        free(inode_bitmap); free(block_bitmap);
        printf("ERR: makeVD %d", err2);
        return err2;
    }

    //writing bit maps
    vd_deb("makeVD writing bitmaps ",0);
    FILE *fp;
    fp=fopen(fileName, "a");
        if(fp == NULL){
            free(sb);
            free(inode_bitmap); free(block_bitmap);
            printf("ERR: makeVD -1");
            return -1;
        }
        fwrite(inode_bitmap, sizeof(int), sb->mInodeCount/BITS_IN_INT, fp);
        fwrite(block_bitmap, sizeof(int), sb->mBlockCount/BITS_IN_INT, fp);

    // writing inodes
        Inode_t *inodes = malloc(INODE_SIZE__BYTE * sb->mInodeCount);
        fwrite(inodes, INODE_SIZE__BYTE, sb->mInodeCount, fp);
    fclose(fp);


    // initialising data blocks
    vd_deb("makeVD init data blocks ",0);
    void *dataBlock = (void*) malloc(BLOCK_SIZE__BIT/8* sb->mBlockCount);

    // writing data blocks
    vd_deb("makeVD writing data blocks ",0);
    fp=fopen(fileName, "a");
    if(fp == NULL){
        free(sb);
        free(inode_bitmap); free(block_bitmap);
        free(dataBlock);
        printf("ERR: makeVD -1");
        return -1;
    }
        fwrite(dataBlock, BLOCK_SIZE__BIT/8* sb->mBlockCount, 1, fp);
    fclose(fp);

    free(sb);
    free(inode_bitmap); free(block_bitmap);
    free(dataBlock);
    vd_deb("makeVD done <<< ",0);
}


int GetVD_BM(char *fileName, int **retBlockMap)
{
    vd_deb("getBM starting ...",0);

    //getting offset
    size_t offset;
    int err1 = GetVD_SB_1BM(fileName,&offset);
    vd_deb("getBM got offset", offset);
    if(err1<0){
        printf("getBM err %d", err1);
        return err1;
    }

    // getting no. of blocks
    unsigned int blockCount;
    int err2 = GetVD_SB_BC(fileName, &blockCount);
    vd_deb("getBM got block count", blockCount);
    if(err2<0){
        printf("getBM err %d", err2);
        return err1;
    }

    //helping variable
    int* x;
    x = (int*) calloc(blockCount/BITS_IN_INT,sizeof(int));

    //reading block map
    FILE *fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            printf("getBM: makeVD -1");
            return -1;
        }
        fseek(fp, offset,SEEK_SET);
        fread(x, sizeof(int), blockCount/BITS_IN_INT, fp);
    fclose(fp);

    (*retBlockMap) = x;
    vd_deb("getBM done <<< ",0);
}


int GetVD_IM(char *fileName, int **retInodeMap)
{
    vd_deb("getIM starting ...",0);

    //getting offset
    size_t offset;
    int err1 = GetVD_SB_1IM(fileName,&offset);
    vd_deb("getIM got offset", offset);
    if(err1<0){
        printf("getIM err %d", err1);
        return err1;
    }

    // getting no. of inodes
    unsigned int inodeCount;
    int err2 = GetVD_SB_IC(fileName, &inodeCount);
    vd_deb("getIM got inode count", inodeCount);
    if(err2<0){
        printf("getIM err %d", err2);
        return err1;
    }

    //helping variable
    int* x;
    x = (int*) calloc(inodeCount/BITS_IN_INT,sizeof(int));

    //reading inode map
    FILE *fp;
    fp=fopen(fileName, "r");
        if(fp == NULL){
            printf("getIM: makeVD -1");
            return -1;
        }
        fseek(fp,  offset,SEEK_SET);
        fread(x, sizeof(int), inodeCount/BITS_IN_INT, fp);
    fclose(fp);

    (*retInodeMap) = x;
    vd_deb("getIM done <<< ",0);
}

int GetVD_BLOCK(char *fileName, void **retBlock, int blockNo)
{
        vd_deb("getVD_BLOCK starting ...",0);

        //getting offset
        size_t offset;
        int err1 = GetVD_SB_1B(fileName,&offset);
        offset += blockNo * BLOCK_SIZE__BIT/8;
        vd_deb("getVD_BLOCK got offset", offset);
        if(err1<0){
            printf("getVD_BLOCK err %d", err1);
            return err1;
        }

        // getting no. of blocks
        unsigned int blockCount;
        int err2 = GetVD_SB_BC(fileName, &blockCount);
        vd_deb("getVD_BLOCK got block count", blockCount);
        if(err2<0){
            printf("getVD_BLOCK err %d", err2);
            return err1;
        }
        if(blockNo >= blockCount){
            printf("getVD_BLOCK err: blockNo >= blockCount");
            return -2;
        }

        //helping variable
        void* x;
        x = (void*) calloc(1,BLOCK_SIZE__BIT/8);

        FILE *fp;
        fp=fopen(fileName, "r");
            if(fp == NULL){
                printf("getVD_BLOCK: makeVD -1");
                return -1;
            }
            fseek(fp, offset,SEEK_SET);
            fread(x, BLOCK_SIZE__BIT/8, 1, fp);
        fclose(fp);

        (*retBlock) = x;
        vd_deb("getVD_BLOCK done <<< ",0);
}

int GetVD_INODE(char *fileName, Inode_t **retInode, int inodeNo)
{
        vd_deb("getVD_INODE starting ...",0);

        //getting offset
        size_t offset;
        int err1 = GetVD_SB_1I(fileName,&offset);
        offset += inodeNo * INODE_SIZE__BYTE;
        vd_deb("getVD_INODE got offset", offset);
        if(err1<0){
            printf("getVD_INODE err %d", err1);
            return err1;
        }

        // getting no. of inodes
        unsigned int inodeCount;
        int err2 = GetVD_SB_IC(fileName, &inodeCount);
        vd_deb("getVD_INODE got block count", inodeCount);
        if(err2<0){
            printf("getVD_INODE err %d", err2);
            return err1;
        }
        if(inodeNo >= inodeCount){
            printf("getVD_INODE err: inodeNo (%d) >=  (%d) inodeCount\n", inodeNo, inodeCount);
            return -2;
        }

        //helping variable
        Inode_t* x;
        x = (Inode_t*) malloc (sizeof(Inode_t));

        FILE *fp;
        fp=fopen(fileName, "r");
            if(fp == NULL){
                printf("getVD_INODE: makeVD -1");
                return -1;
            }
            fseek(fp, offset,SEEK_SET);
            fread(x, INODE_SIZE__BYTE, 1, fp);
        fclose(fp);

        (*retInode) = x;
        vd_deb("getVD_INODE done <<< ",0);
}


int GetVD_VFILE_INFO(char *fileName, char *vFileName,
                     int *retFileNo, int *retBlockCount, int *retInodeNo)
{
    vd_deb("getVD_VFILE_INFO starting ...",0);
    int *inodeMap;
    int err1 = GetVD_IM(fileName, &inodeMap);

    int inodeCount;
    int err2 = GetVD_SB_IC(fileName, &inodeCount);
    vd_deb("getVD_VFILE_INFO inode count ",inodeCount);


//    printf("\n==\n");
//    printBitTable(inodeMap, inodeCount);
//    printf("\n==\n");


    if(err1<0 || err2<0){
           printf("ERR getVD_VFILE_INFO");
           return -1;
    }

    int i;
    for(i=0; i<inodeCount; i++){
        if(TestBit(inodeMap, i)){
            vd_deb("\n\n\n found some file!\n\n\n",i);
            Inode_t* inode;
            int err = GetVD_INODE(fileName,&inode, i);
            if(err < 0){
                printf("ERR getVD_VFILE_INFO");
                free(inode);
                return err1;
            }
            if(inode == NULL){
                printf("ERR getVD_VFILE_INFO");
                return -1;
            }

            if(strcmp(inode->mFileName,vFileName) == 0){
                vd_deb("getVD_VFILE_INFO file found (inode no)",i);

                *retFileNo = inode->mFileBegin;
                *retBlockCount = inode->mFileSize__byte/(BLOCK_SIZE__BIT/8);
                if(inode->mFileSize__byte%(BLOCK_SIZE__BIT/8)!= 0)
                    (*retBlockCount)++;
                *retInodeNo = i;

                free(inode);
                return 0;
            }
            free(inode);
        }
    }
    vd_deb("getVD_VFILE_INFO file not found",-1);

    free(inodeMap);
    vd_deb("getVD_VFILE_INFO done <<< ",-1);
    return -1;
}


int FindVD_FREE_BLOCKS(char* fileName, int count, int *ret1stBlockNo)
{
    vd_deb("FindVD_FREE_BLOCKS starting ...",0);

    int *blockMap;
    int err1 = GetVD_BM(fileName, &blockMap);

    int blockCount;
    int err2 = GetVD_SB_BC(fileName, &blockCount);

    if(err1<0 || err2<0){
        free(blockMap);
        printf("ERR getVD_VFILE_INFO");
        return -1;
    }

    int sum = 0;
    int i;
    for(i=0; i<blockCount; i++){
        if(!TestBit(blockMap, i))
            sum++;
        else
            sum = 0;

        if(sum ==count){
            vd_deb("FindVD_FREE_BLOCKS got free blocks",i);
            free(blockMap);
            (*ret1stBlockNo )= i - (count-1);
            return 1;
        }
    }

    vd_deb("FindVD_FREE_BLOCKS done <<< ",-1);
    free(blockMap);
    return -1;
}


int CopyFileToVD(char *fileName, char *inputName, char *newVFileName)
{
    vd_deb("\n\ncopyFileToVD starting ...",0);

    //open file to copy
    FILE *fp;
    fp=fopen(inputName, "r");

        //check size
        fseek(fp, 0L, SEEK_END);
        size_t sz = ftell(fp);
        fseek(fp, 0L, SEEK_SET);
        vd_deb("CopyFileToVD: computed size of file", sz);

        void* data = (void*) malloc(sz);
        fread(data, sz, 1, fp);
    close(fp);

    //check if we can store this file
    size_t freeSize;
    GetVD_SB_FREE(fileName, &freeSize);
    vd_deb("CopyFileToVD: computed freeSize of VD", freeSize);
    if(freeSize < sz){
        printf("not enough size !");
        return -1;
    }

    //check if we need defragmentation
    int blockCountNeeded = sz/(BLOCK_SIZE__BIT/8);
    if(sz%(BLOCK_SIZE__BIT/8)!= 0)
        blockCountNeeded ++;

    vd_deb("CopyFileToVD: computed blockCountNeeded", blockCountNeeded);
    int blockNo;
    int err = FindVD_FREE_BLOCKS(fileName, blockCountNeeded,  &blockNo);
    vd_deb("CopyFileToVD: free blocks starts at block no", blockNo);

    if(err < 0 ){
        printf("need to defragment!");
        return -1;
    }

    // finding free inode
    int freeInodeNum;
    FindVD_FREE_INODE(fileName,&freeInodeNum);
    if(freeInodeNum < 0){
        printf("CopyFileToVD: no free inode");
    }

    //set inodeMap
    SetVD_INODE_BIT(fileName, freeInodeNum, 1);

    //set blockMap
    SetVD_BM_BITS(fileName, blockNo, blockCountNeeded, 1);

    //set up inode
    Inode_t new_inode;
    new_inode.mFileBegin = blockNo;
    new_inode.mFileSize__byte = sz;
    memcpy(new_inode.mFileName,newVFileName, sizeof(char) * FILE_NAME_MAX_SIZE);

    //<TODO> check error codes...
    size_t first_inode;
    if(GetVD_SB_1I(fileName, &first_inode)<0){
        printf("CopyFileToVD: ERR a\n");
        return -1;
    }

//        size_t first_inode_map;
//        GetVD_SB_1IM(fileName, &first_inode_map);

    size_t firstBlock;
    if(GetVD_SB_1B(fileName, &firstBlock)<0){
        printf("CopyFileToVD: ERR b\n");
        return -1;
    }


    FILE* vf;
    vf = fopen(fileName, "r+");
        if(vf == NULL){
            return -1;
        }
        // writing inode
        fseek(vf,first_inode + freeInodeNum*INODE_SIZE__BYTE,SEEK_SET);
        fwrite(&new_inode, INODE_SIZE__BYTE, 1, vf);

        //paste to data blocks
        fseek(vf, firstBlock+blockNo * BLOCK_SIZE__BIT/8, SEEK_SET);
        fwrite(data, sz,1,vf);
    fclose(vf);
    // updating free size
    ChangeVD_SB_FREE(fileName, -1 * blockCountNeeded);
    printf("added %s in %d blocks (%.2f kB)\n", newVFileName, blockCountNeeded, blockCountNeeded*BLOCK_SIZE__BIT/(float)8000);
    vd_deb("copyFileToVD done <<< ",0);
}


int CopyFileFromVD(char *fileName, char *inputName, char *newFileName)
{
    int fileNo, inodeNo, blockCount;

    GetVD_VFILE_INFO(fileName, inputName, &fileNo,&blockCount, &inodeNo );

    // reading inode
    Inode_t *inode = (Inode_t*) malloc(sizeof(Inode_t));
    GetVD_INODE(fileName, &inode, inodeNo);

    // getting offset
    size_t offset;
    GetVD_SB_1B(fileName, &offset);

    offset += inode->mFileBegin*BLOCK_SIZE__BIT*8;


    // reading data
    void* data = (void* ) malloc(inode->mFileSize__byte);
    FILE* vf;
    vf= fopen(fileName, "r");
        fseek(vf, offset, SEEK_SET);
        fread(data, inode->mFileSize__byte, 1, vf);
    fclose(vf);

    FILE* newFile;
    newFile = fopen(newFileName,"w");
        fseek(newFile, 0, SEEK_SET);
        fwrite(data, inode->mFileSize__byte, 1, newFile);
    fclose(newFile);

    free(inode);
}


int SetVD_BM_BITS(char *fileName, int blockNo, int numberOfBits, int setBool)
{
    //getting BM
    int *BM;
    if(GetVD_BM(fileName,&BM)<0){
        printf("SetVD_BM_BITS: ERR");return -1;
    }

    //producing new BM
    int i;
    for(i = blockNo; i<blockNo+numberOfBits; i++){
        if(1 == setBool){
            SetBit(BM,i);
        }else{
            ClearBit(BM, i);
        }
    }

    //getting offset
    size_t offset;
    if(GetVD_SB_1BM(fileName,&offset)<0){
        free(BM);
        printf("SetVD_BM_BITS: ERR");return -1;
    }

    //getting block count
    unsigned int blockCount;
    if(GetVD_SB_BC(fileName, &blockCount)<0){
        free(BM);
        printf("SetVD_BM_BITS: ERR");return -1;
    }

    //saving new BM to VD
    FILE *vf;
    vf = fopen(fileName, "r+");
        fseek(vf, offset, SEEK_SET);
        fwrite(BM,sizeof(int), blockCount/BITS_IN_INT, vf);
    fclose(vf);

    free(BM);
    return 0;
}


int SetVD_INODE_BIT(char *fileName, int inodeNo, int setBool)
{
    //getting IM
    int *IM;
    if(GetVD_IM(fileName,&IM)<0){
        printf("SetVD_INODE_BIT: ERR");return -1;
    }

    //producing new IM
    if(1 == setBool){
        SetBit(IM,inodeNo);
    }else{
        ClearBit(IM, inodeNo);
    }

    //getting offset
    size_t offset;
    if(GetVD_SB_1IM(fileName,&offset)<0){
        free(IM);
        printf("SetVD_INODE_BIT: ERR");return -1;
    }

    //getting inode count
    unsigned int inodeCount;
    if(GetVD_SB_IC(fileName, &inodeCount)<0){
        free(IM);
        printf("SetVD_INODE_BIT: ERR");return -1;
    }

    //saving new IM to VD
    FILE *vf;
    vf = fopen(fileName, "r+");
        fseek(vf, offset, SEEK_SET);
        fwrite(IM,sizeof(int), inodeCount/BITS_IN_INT, vf);
    fclose(vf);

    free(IM);
    return 0;
}


int FindVD_FREE_INODE(char *fileName, int *retInodeNo)
{
    //find inode
    int* inodeMap;
    int err;
    err = GetVD_IM(fileName, &inodeMap);
    if(err < 0){
        printf("ERR: FindVD_FREE_INODE: getVD_IM\n");
        return -1;
    }

    int inodeCount;
    err = GetVD_SB_IC(fileName, &inodeCount);
    if(err < 0){
        printf("ERR: FindVD_FREE_INODE: GetVD_SB_IC");
        free(inodeMap);
        return -1;
    }
    vd_deb("FindVD_FREE_INODE: InodeCount:", inodeCount);


    int i;
    for(i =0; i < inodeCount; i++){
        if(TestBit(inodeMap, i) == 0){
            (*retInodeNo) = i;
            return 0;
        }
    }
    printf("ERR: FindVD_FREE_INODE: no free inode");
    free(inodeMap);
    return -1;
}


int DeleteVD_VFILE(char *fileName, char *vFileNameToDelete)
{
    int blockCount, blockNo, inodeNo;
    if(GetVD_VFILE_INFO(fileName,vFileNameToDelete,&blockNo,&blockCount,&inodeNo)<0){
        vd_deb("ERR: DeleteVD_VFILE, cannot get info",-1);return -1;
    }

    if(SetVD_INODE_BIT(fileName, inodeNo, 0)<0){
        vd_deb("ERR: DeleteVD_VFILE, cennot delete inode bit",-2);return -2;
    }

    if(SetVD_BM_BITS(fileName,blockNo,blockCount,0)<0){
        vd_deb("ERR: DeleteVD_VFILE, cennot delete inode bit",-3);return -3;
    }

    if(ChangeVD_SB_FREE(fileName, blockCount)<0){
        vd_deb("ERR: DeleteVD_VFILE, cennot change free size",-4);return -4;
    }
    printf("removed %s, freed %d blocks(%.2f kB)\n", vFileNameToDelete, blockCount, blockCount*BLOCK_SIZE__BIT/(float)8000);
    return 0;
}


int GetVD_FILES_NAMES(char *fileName, char **retList, int *retNumber)
{
//    int maxFiles = 30;//max 30 files returned (just for testing)

    int* IM;
    (*retNumber) = 0;
    //retList = calloc(maxFiles, FILE_NAME_MAX_SIZE*sizeof(char));
    char *str1; str1 = calloc(FILE_NAME_MAX_SIZE+1, sizeof(char)); str1[0] = '\0';
    char *str2; str2 = calloc(FILE_NAME_MAX_SIZE+1, sizeof(char)); str1[0] = '\0';

    if(GetVD_IM(fileName, &IM) < 0){
        free(str1); free(str2);
        printf("ERR: GetVD_FILES_NAMES, cannot get IM\n");return -1;
    }

    int inodeCount;
    if(GetVD_SB_IC(fileName, &inodeCount)<0){
        free(str1); free(str2);
        printf("ERR: GetVD_FILES_NAMES, cannot get inodeCount\n");return -2;
    }

    int i;
    char* newline = "\n";
    for(i =0; i< inodeCount ; i++){

        Inode_t *tempInode;

        if(TestBit(IM, i) == 1){
            //found a file

            if(GetVD_INODE(fileName, &tempInode,i)<0){
                free(str1); free(str2);
                free(tempInode);
                printf("ERR: GetVD_FILES_NAMES, cannot get inode");return -3;
            }

            (*retNumber)++;

            if(*retNumber == 1){
                strcat(str1, tempInode->mFileName);
                strcat(str1, newline);
            }else{
                free(str2);
                str2 = str1;
                str1 = calloc((FILE_NAME_MAX_SIZE+1)*(*retNumber), sizeof(char)); str1[0] = '\0';
                str1[0] = '\0';
                strcat(str1, str2);
                strcat(str1, tempInode->mFileName);
                strcat(str1, newline);
            }

            // adding string
            free(tempInode);
        }
    }
    (*retList) = str1;
    free(str2);
    return 0;
}


int ExistVD_File(char *fileName, char *nameToFind, int *retBool)
{
    (*retBool) = 0;
    int *inodeMap;
    int err1 = GetVD_IM(fileName, &inodeMap);

    int inodeCount;
    int err2 = GetVD_SB_IC(fileName, &inodeCount);

    if(err1<0 || err2<0){
           printf("ERR getVD_VFILE_INFO");
           return -1;
    }

    int i;
    for(i=0; i<inodeCount; i++){
        if(TestBit(inodeMap, i)){

            Inode_t* inode;
            int err = GetVD_INODE(fileName,&inode, i);
            if(err < 0){
                printf("ERR getVD_VFILE_INFO");
                free(inode);
                return err1;
            }
            if(inode == NULL){
                printf("ERR getVD_VFILE_INFO");
                return -1;
            }

            if(strcmp(inode->mFileName,nameToFind) == 0){
                (*retBool) = 1;
                return 0;
            }
            free(inode);
        }
    }
    free(inodeMap);
    return 0;
}
