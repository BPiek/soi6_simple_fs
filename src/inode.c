#include "inode.h"

//#define DEBUG_INODE

    #include <stdio.h>

void inode_deb(char* c, int code){
    #ifdef DEBUG_INODE
        printf("[INODE] (%d): %s\n", code, c);
    #endif
}
