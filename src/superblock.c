#include "superblock.h"


int initSuperBlock(SuperBlock_t *sb, size_t size, char *name) //size in bits
{
    inode_deb("initSuperBlock starting...",0);
    int code = 1;

    //checking wrong input...
    if(size < BLOCK_SIZE__BIT*10 + BOOL_SIZE__BIT*20 + INODE_SIZE__BYTE*10*8 + SUPER_BLOCK_SIZE__BYTE*8){
        code = -1;
        inode_deb("size to small", code);
        return code;
    }

    //seting mId
    strncpy(sb->mId, name, MAX_DISK_ID_SIZE-1);
    sb->mId[MAX_DISK_ID_SIZE] = '\0';

    //conting counts of inodes,blocks
    size_t n;   // n- number of blocks. Number of inodes is also n so we can
                // compute n easier. We can have also disk filled with files
                // of size <= size of block
    n = (size - SUPER_BLOCK_SIZE__BYTE*8)/(2*BOOL_SIZE__BIT + INODE_SIZE__BYTE*8 + BLOCK_SIZE__BIT);
                // We have:
                //      super block (only one),
                //      1x arrays of "bools" for maping inodes that
                //          are taken (n entities),
                //      1x aray of "bools" for same thing for blocks (n entities),
                //      1x array of inodes (n entities)
                //      1x array of blocks (n entities) for "final" data storage.
                // all those summs up to "size"

    // We agree to make slightly bigger disk then given size
    // in order to make things easier (we store bit maps in int
    // so if we wouldn't o this there would be bits that does not
    // describe any inode/block therefor we would have to store additional
    // variables and make additional tests

    while(n%BITS_IN_INT != 0){
        n++;
    }
    // computing new size
    sb->mSize__bit = SUPER_BLOCK_SIZE__BYTE*8 + n*(2*BOOL_SIZE__BIT + INODE_SIZE__BYTE*8 + BLOCK_SIZE__BIT);

    inode_deb("initSuperBlock: new size computed", n);

    sb->mBlockCount = n;
    sb->mInodeCount = n;


    // setting free size
    sb->mFreeSize__bit = sb->mBlockCount * BLOCK_SIZE__BIT;

    sb->m1stInodeMap = SB_OFFSET_END;
//    sb->m1stInodeMap =  sb->m1stBlockMap + n/BITS_IN_INT*sizeof(int);
//    sb->m1stInode = sb->m1stInodeMap + n/BITS_IN_INT*sizeof(int);
//    sb->m1stBlock = sb->m1stInode + INODE_SIZE__BYTE*n;

    return code;
}

int writeSuperBlock_t(SuperBlock_t *sb)
{
    inode_deb("writeSuperBlock_t starting...",0);

    FILE *fp;
    fp=fopen("superblock_test", "w");
        int x[2], y[3];
        x[0] = 1;
        x[1] = 3;

        y[0] = 7;
        y[1] = 11;
        y[2] = 17;

        fwrite(x, sizeof(x[0]), NELEMS(x), fp);
        fwrite(y, sizeof(y[0]), NELEMS(y), fp);
        fseek(fp,0,SEEK_SET);
        fwrite(y, sizeof(y[0]), NELEMS(y), fp);
    fclose(fp);
    inode_deb("writeSuperBlock done",0);
}


int printSuperBlock(SuperBlock_t *sb)
{
    int code =0;
    if(sb == NULL){
        code = -1;
        inode_deb("printSuperBlock: null ptr", code);
        return code;
    }
    printf("printing sb...\n");
    printf("\tname: %s\n", sb->mId);
    printf("\tsize: %d\n", sb->mSize__bit);
    return code;
}


int printFromFileSuperBlock_t()
{
    int code =0;
    inode_deb("printFromFileSuperBlock starting...",0);

    FILE * fp;
    fp=fopen("superblock_test", "r");
        int y[5];
        fread(y, sizeof(y[0]), NELEMS(y), fp);
        int i;
        for(i = 0; i< NELEMS(y); i++){
            printf("%d: %d\n",i,y[i]);
        }
    fclose(fp);
    return code;
}


int writeSuperBlock(SuperBlock_t *sb, char *fileName)
{
    inode_deb("writeSuperBlock_t starting...",0);

    FILE *fp;
    fp=fopen(fileName, "w");
        fseek(fp,0,SEEK_SET);
        fwrite(sb->mId, sizeof(sb->mId),1,fp);

        fwrite( &(sb->mSize__bit), sizeof(sb->mSize__bit),1,fp);
        fwrite( &(sb->mFreeSize__bit), sizeof(sb->mFreeSize__bit),1,fp);

        fwrite( &(sb->mBlockCount), sizeof(sb->mBlockCount),1,fp);
        fwrite( &(sb->mInodeCount), sizeof(sb->mInodeCount),1,fp);

        fwrite( &(sb->m1stBlockMap), sizeof(sb->m1stBlockMap),1,fp);
        fwrite( &(sb->m1stInodeMap), sizeof(sb->m1stInodeMap),1,fp);

        fwrite( &(sb->m1stBlock), sizeof(sb->m1stBlock),1,fp);
        fwrite( &(sb->m1stInode), sizeof(sb->m1stInode),1,fp);
    fclose(fp);

    inode_deb("writeSuperBlock done",0);
}
