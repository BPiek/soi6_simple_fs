#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

//Dysk wirtualny

//+--------------------+----------------+-----------------+--------------------+---------------------------------+
//|                    |                |                 |                    | +---------------+        +------|
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       | tablica|     ||
//|                    |                |                 |                    | |  blok | blok  | bloków |blok ||
//|    Super Block     |   Tablica      |     Tablica     |    Tablica         | |  1    | 2     |        | n   ||
//|                    |   zajętości    |     zajętości   |    inode-ów        | |       |       |        |     ||
//|                    |   inode-ów     |     blków       |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       | ...... |     ||
//|                    |    (1-zajęty   |    (1-zajęty,   |                    | |       |       |        |     ||
//|                    |    0-wolny)    |    0-wolny)     |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | |       |       |        |     ||
//|                    |                |                 |                    | +---------------+        +------|
//+--------------------+----------------+-----------------+--------------------+---------------------------------+


char *datetime()
{
    char *array = (char*)malloc(sizeof(char)*25);
    time_t result;
    result = time(NULL);
    sprintf(array, "%s", asctime(localtime(&result)));
    array[25] = '\0';
    return array;
}

char *getDateTime()
{
    char *array = (char*)malloc(sizeof(char)*20);
    memset (array, 0, 20);
    time_t rawtime;
    rawtime = time(NULL);
    struct tm  *timeinfo = localtime (&rawtime);
    strftime(array, 20, "%d.%m.%y_%H:%M:%S", timeinfo);
    array[20] = '\0';
    return array;
}
void t1();
void t2();
void t3();
void t4();
void t5();
void t6(), t7(), t8(), t9();
int main(void)
{
    t6();
//    size_t x =  sizeof(char) * FILE_NAME_MAX_SIZE +
//                sizeof(fileSize_t) +
//                sizeof(iter_t);
//    printf("x: %d, inode: %d", x,INODE_SIZE__BYTE);
    return(0);
}
void t1(){
    // some date tests
    // prints Sat Aug  3 18:39:07 2013
    printf("%s", datetime());

    // how to print:
    // 03.08.2013_18:39:07
    // ?
    printf("\n\n%s\n\n", getDateTime());
}

void t2(){
    // some file tests
    FILE *fp;
    fp=fopen("testxxx", "wb");
    char x[10]="ABCDEFGH";
    fwrite(x, sizeof(x[0]), sizeof(x)/sizeof(x[0]), fp);
    fclose(fp);
    fp= fopen("testxxx", "a");
    fwrite(x, sizeof(x[0]), sizeof(x)/sizeof(x[0]), fp);
    fclose(fp);
}

void t3(){
    //som sb tests
    SuperBlock_t *sb = (SuperBlock_t*) malloc(sizeof(SuperBlock_t));
    char name[MAX_DISK_ID_SIZE] = "some_name";
    initSuperBlock(sb, BLOCK_SIZE__BIT*50, name);
    printSuperBlock(sb);
    writeSuperBlock_t(sb);
    printFromFileSuperBlock_t();
}

void t4(){
    int *z;
    int size = createBitTable(BITS_IN_INT*1, &z);
    printBitTable(z, size);
    SetBit(z,20);
    SetBit(z,2);
    SetBit(z,16);
//    ClearBit(z,10);
//    clearBitTable(z, size);
    printBitTable(z, size);


//    createBitTable(64, x);
//    int i;
//    for(i = 0; i < NELEMS(x); i++)
//        printf("%d ", x[i]);
//    clearBitTable(x);
//    printBitTable(x);
}
void t5(){
    SuperBlock_t *sb = (SuperBlock_t*) malloc(sizeof(SuperBlock_t));
    char name[MAX_DISK_ID_SIZE] = "some_name";
    char plik[10] = "test5";
    initSuperBlock(sb, BLOCK_SIZE__BIT*50, name);
    printSuperBlock(sb);

    char ret[MAX_DISK_ID_SIZE];
    writeSuperBlock(sb,plik);
    GetVD_SB_ID(plik, ret);
    printf("\n<<%s>>\n", ret);

    size_t* x = (size_t*) malloc(sizeof(size_t));
    GetVD_SB_SIZE(plik,x);
    printf("\n<<<%i>>>\n", (*x));


    ChangeVD_SB_FREE(plik, -1);
    GetVD_SB_FREE(plik,x);
    printf("\n<<<%i>>>\n", (*x));

}

void t6(){
    char name[MAX_DISK_ID_SIZE] = "some_name";
    char plik[10] = "vd";

    int errno = makeVD(plik, BLOCK_SIZE__BIT*80, name);
//
//    unsigned int inodeCount;

//    GetVD_SB_IC(plik,&inodeCount);
//    printf("\n\ngot inode count: %d\n\n", inodeCount);

//    int* inodeMap;
//    GetVD_IM(plik, &inodeMap);
//    printBitTable(inodeMap, inodeCount);

//    free(inodeMap);
//


    int* block_map;
    unsigned int blockCount;
    GetVD_SB_BC(plik, &blockCount);
    GetVD_BM(plik, &block_map);
    printBitTable(block_map, blockCount);

    char plikIstniejacy[10] = "plik1";
    char nazwaWVFS[FILE_NAME_MAX_SIZE ] = "plikCopy";
    char nowaNazwa[15] = "plik1_copy";
    size_t l,r;

    GetVD_SB_SIZE(plik, &l);
    CopyFileToVD(plik, plikIstniejacy, nazwaWVFS);
    GetVD_SB_SIZE(plik, &r);

    printf("\n\n%d and%d",l,r);


    CopyFileFromVD(plik, nazwaWVFS, nowaNazwa);

}
void t7(){
    char name[MAX_DISK_ID_SIZE] = "some_name";
    char plik[10] = "vd2";
    char plikIstniejacy[10] = "plik1";
    char nazwaWVFS[FILE_NAME_MAX_SIZE ] = "plikCopy";

    int errno = makeVD(plik, BLOCK_SIZE__BIT*80, name);

    unsigned int inodeCount;
    GetVD_SB_IC(plik,&inodeCount);
    printf("\n\ngot inode count: %d\n\n", inodeCount);
    int* inodeMap;
    GetVD_IM(plik, &inodeMap);
    printBitTable(inodeMap, inodeCount);

    CopyFileToVD(plik, plikIstniejacy, nazwaWVFS);

    GetVD_SB_IC(plik,&inodeCount);
    printf("\n\ngot inode count: %d\n\n", inodeCount);
    GetVD_IM(plik, &inodeMap);
    printBitTable(inodeMap, inodeCount);
}
void t8(){
    //check makeVD and getters
    char name[MAX_DISK_ID_SIZE] = "some_name";
    char plik[10] = "vd_t8";

    //making VD
    int errno = makeVD(plik, BLOCK_SIZE__BIT*80, name);

    //geting info
    unsigned int inodeC, blockC;
    GetVD_SB_IC(plik,&inodeC);
    GetVD_SB_BC(plik, &blockC);

    //getting maps
    int *inodeM, *blockM;
    GetVD_IM(plik, &inodeM);
    GetVD_BM(plik, &blockM);

    //printing maps
    printBitTable(inodeM, inodeC);
    printf("\n\n");
    printBitTable(blockM, blockC);
}

void t9(){

}
