#ifndef inode_lib
#define inode_lib

#include <time.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"


#define MAX_DISK_ID_SIZE    64
#define FILE_NAME_MAX_SIZE  64

// sizes
#define BLOCK_SIZE__BIT     2048

typedef struct Inode
{
    char mFileName[FILE_NAME_MAX_SIZE];

    //file details
    fileSize_t mFileSize__byte;
    iter_t mFileBegin;
//    time_t mFileLastChanged;

    //inode details
//    time_t mInodeLastChanged;

    //flags
//    short exist;
} Inode_t;
static size_t INODE_SIZE__BYTE = sizeof(Inode_t);

#endif //inode_lib
