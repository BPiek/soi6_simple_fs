#ifndef inode_superblock_lib
#define inode_superblock_lib

#include <stdio.h>
#include <stdlib.h>

#include "inode.h"

static size_t BOOL_SIZE__BIT = 1;

typedef struct SuperBlock{
    // info about disk
    char mId[MAX_DISK_ID_SIZE];

    size_t mSize__bit; // size in [?]
    size_t mFreeSize__bit;

    unsigned int mBlockCount; // total number of blocks in disk
    unsigned int mInodeCount; // total number of inodes in disk

    size_t m1stBlockMap;
    size_t m1stInodeMap;

    size_t m1stInode;
    size_t m1stBlock;
} SuperBlock_t;
static size_t SUPER_BLOCK_SIZE__BYTE = sizeof(SuperBlock_t);

static int SB_OFFSET_END =
        sizeof(char) * MAX_DISK_ID_SIZE +
        sizeof(size_t) * 6 +
        sizeof(unsigned int) * 2;

int initSuperBlock(SuperBlock_t *sb, size_t size, char *name);
int printSuperBlock(SuperBlock_t *sb);
int writeSuperBlock(SuperBlock_t *sb, char* fileName);

//some tests, can be stupid
int writeSuperBlock_t(SuperBlock_t *sb); // for testing only
int printFromFileSuperBlock_t(); // for testing only
#endif //inode_superblock_lib
