#ifndef vdisk_lib
#define vdisk_lib

#include "superblock.h"
#include "inode.h"
#include "types.h"


// reading/writing from VS- SUPER BLOCK MEMBERS:
int GetVD_SB_ID(char *fileName, char* retName); // get VD's (virtual disk's) member of SB (special block) named ID (mId)

int GetVD_SB_SIZE(char *fileName, size_t* ret); //size of VD
int GetVD_SB_FREE(char *fileName, size_t* ret); //free size

int GetVD_SB_BC(char *fileName, unsigned int* ret); //block count
int GetVD_SB_IC(char *fileName, unsigned int* ret); //inode count

int GetVD_SB_1BM(char *fileName, size_t* ret); //1st block map
int GetVD_SB_1IM(char *fileName, size_t* ret); //1st inode map

int GetVD_SB_1B(char *fileName, size_t* ret); //1st block
int GetVD_SB_1I(char *fileName, size_t* ret); //1st inode


int ChangeVD_SB_FREE(char *fileName, long blocks_number);

// making disk
///
/// \brief makeVD
/// \param fileName name of file that will be our VD file in linux fs
/// \param size
/// \param name name of our disk (name stored in SB
/// \return err code
///
int makeVD(char *fileName, size_t size, char* name);


// this vFS wont be close to optimal
// for simplicity while chaneging sth it will
// replace whole bit maps for new ones

int GetVD_BM(char* fileName, int **retBlockMap);
int GetVD_IM(char* fileName, int **retInodeMap);

int GetVD_BLOCK(char* fileName, void **retBlock, int blockNo);
int GetVD_INODE(char* fileName, Inode_t **retInode, int inodeNo);

int GetVD_VFILE_INFO(char* fileName,
                     char* vFileName,
                     int* retFileNo,
                     int* retBlockCount,
                     int* retInodeNo); // info about file (info stored in inode)
int FindVD_FREE_BLOCKS(char *fileName, int count, int *ret1stBlockNo);
int FindVD_FREE_INODE(char* fileName, int *retInodeNo);

int CopyFileToVD(char* fileName, char* inputName, char* newVFileName);
int CopyFileFromVD(char* fileName, char* inputName, char* newFileName);

int SetVD_BM_BITS(char* fileName, int blockNo, int numberOfBits, int setBool);
int SetVD_INODE_BIT(char* fileName, int inodeNo, int setBool);

int DeleteVD_VFILE(char* fileName, char *vFileNameToDelete);
int ExistVD_File(char* fileName, char* nameToFind, int* retBool);
///
/// \brief GetVD_FILES_NAMES
/// \param fileName VD file name
/// \param retList
/// \param retNumber count of file names returned
/// \return errNo
///
int GetVD_FILES_NAMES(char* fileName, char** retList, int* retNumber);


#endif //vdisk_lib
