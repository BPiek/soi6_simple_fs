#ifndef inode_types_lib
#define inode_types_lib

#define BITS_IN_INT (sizeof(int)*8)
//warning: NELEMS only for static arrays!
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0]))

typedef unsigned long fileSize_t;
typedef unsigned long iter_t;

//implementing bit array from
//http://www.mathcs.emory.edu/~cheung/Courses/255/Syllabus/1-C-intro/bit-array.html

int createBitTable(int size, int** table);
void SetBit(int *A,  int k );
void ClearBit(int *A,  int k );
int TestBit(int *A,  int k );
void clearBitTable(int *A, int size);
void printBitTable(int *A, int size);


#endif //inode_types_lib
