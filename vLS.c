#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 2){
        printf("usage: \n\tvls   <filename(VD)>   \n");
        fflush(stdin);
        return -1;
    }
    char *vd;

    vd = argv[1];
    if( access( vd, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"(VD)! \n", vd);
        return -1;
    }

    char* files;
    int filesNo;
    while(1){
        if(GetVD_FILES_NAMES(vd, &files,&filesNo)<0){
            printf("cannot check files in VD\"%s\"! \n", vd);
//            return -1;
            continue;
        }
        break;
    }


    printf("%s\n", files);

    return(0);
}
