#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 3){
        printf("usage: \n\tvadd   <filename(VD)>   <filename(file to copy>\n");
        printf("\tvadd   <filename(VD)>   <filename(file to copy)>   <new file's name in VD>\n");
        fflush(stdin);
        return -1;
    }

    char *vd;
    vd = argv[1];
    if( access( vd, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"! (VD)\n", vd);
        return -1;
    }

    char *file;
    file = argv[2];
    if( access( file, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"! (file to copy)\n", file);
        return -1;
    }
    char* newName;
    if(argc == 4)
        newName = argv[3];
    else
        newName = file;

    int fileExistsInVD;
    if(ExistVD_File(vd,newName, &fileExistsInVD) <0){
        printf("cannot check if file %s (new name: %s) exits in vd %s\n", file, newName, vd);
        return -1;
    }

    if(fileExistsInVD){
        printf("cannot add file %s (new name: %s) to vd %s (file exits)\n", file, newName, vd);
        return -1;
    }

    if(CopyFileToVD(vd, file, newName)<0){
        printf("cannot add file %s (new name: %s) to vd %s\n", file, newName, vd);
        return -1;
    }

    return(0);
}
