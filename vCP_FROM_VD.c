#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "inode.h"
#include "superblock.h"
#include "vdisk.h"

int main(int argc, char *argv[])
{
    printf("\n");
    if(argc < 3){
        printf("usage: \n\tvadd   <filename(VD)>   <filename(file to copy from VD>\n");
        printf("\tvadd   <filename(VD)>   <filename(file to copy from VD)>   <new file's name>\n");
        fflush(stdin); return -1;
    }

    char *vd;
    vd = argv[1];
    if( access( vd, F_OK ) == -1 ) {
        printf("cannot get access to \"%s\"! (VD)\n", vd); return -1;
    }

    char *file;
    file = argv[2];

    char* newName;
    if(argc == 4)
        newName = argv[3];
    else
        newName = file;
    if( CopyFileFromVD(vd,file,newName) <0 ){
        printf("cannot copy file %s from vd %s, file does not exist\n", file, vd); return -1;
    }

    return(0);
}
